$(document).ready(function (){

    $('#btnNav').click(function() {

        let menu = $("#btnMenu").hide();
        
        //Quand on appuit sur le bouton 
        //L'image des téléphones disparait et le menu s'affiche

        if(menu){

            $(".phones").hide();
            $("#btnMenu").show();
            $('#btnNav2').show();
            $('#btnNav').hide();
            $('#close').show();
            $('.fondResponsive').css("box-shadow", "inset 0px 0px 30px 30px rgba(0, 0, 0, 0.4)");
            $('.imgFond').css("box-shadow", "inset 3px 50px 35px 2px rgba(0, 0, 0, 0.4)");
        }
    
    });

    $('#btnNav2').click(function() {

        let menu2 = $("#btnMenu").show();
        
        //Quand on appuit sur le bouton le menu disparait et l'image des téléphones réapparait

        if(menu2){

            $(".phones").show();
            $("#btnMenu").hide();
            $('#btnNav2').hide();
            $('#btnNav').show();
            $('#close').hide();
            $('.fondResponsive').css("box-shadow", "0px 0px 0px #000000");
            $('.imgFond').css("box-shadow", "0px 0px 0px #000000");

        }
    
    });

    //Si la fenêtre repasse sur grand écran annule les ombres et affiche l'image des téléphones

    $(window).resize(function() {

        let largeur_fenetre = $("body").width();
        //console.log(largeur_fenetre);


        if(largeur_fenetre > 380){
                $(".phones").show();
                $("#btnMenu").hide();
                $('#btnNav2').hide();
                $('#btnNav').show();
                $('#close').hide();
                $('.fondResponsive').css("box-shadow", "0px 0px 0px #000000");
                $('.imgFond').css("box-shadow", "0px 0px 0px #000000");
            }    
});

    


    

});